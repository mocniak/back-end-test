<?php

namespace Tests\Unit\Entity;

use App\Entity\Exception\SurveyNotAllowingAnswersException;
use App\Entity\Exception\SurveyLifecycleException;
use App\Entity\Survey;
use PHPUnit\Framework\TestCase;
use Tests\Kit\SurveyMother;

class SurveyTest extends TestCase
{
    public function testNewSurveyIsInNewState()
    {
        $newSurvey = SurveyMother::createAny();
        self::assertEquals(Survey::STATUS_NEW, $newSurvey->status());
    }

    public function testSurveyHasToBeCreatedWithNameAndReportEmail()
    {
        $newSurvey = new Survey('Some Name', 'some-email@example.com');
        self::assertEquals('Some Name', $newSurvey->name());
        self::assertEquals('some-email@example.com', $newSurvey->reportEmail());
    }

    public function testNewSurveyCanGoLive()
    {
        $newSurvey = SurveyMother::createAny();
        $newSurvey->goLive();
        self::expectNotToPerformAssertions();
    }

    public function testLiveSurveyCannotGoLiveAgain()
    {
        $liveSurvey = SurveyMother::createLive();
        self::expectException(SurveyLifecycleException::class);
        $liveSurvey->goLive();
    }

    public function testClosedSurveyCannotGoLive()
    {
        $closedSurvey = SurveyMother::createClosed();
        self::expectException(SurveyLifecycleException::class);
        $closedSurvey->goLive();
    }

    /**
     * @dataProvider surveysToBeAnsweredDataProvider
     */
    public function testSurveyCanBeAnsweredWhenIsLiveOnly(Survey $survey, bool $shouldFail)
    {
        if ($shouldFail) {
            self::expectException(SurveyNotAllowingAnswersException::class);
        }
        $survey->addAnswer(0, 'some comment');
        self::expectNotToPerformAssertions();
    }

    public static function surveysToBeAnsweredDataProvider(): array
    {
        return [
            'new survey' => [SurveyMother::createNew(), true],
            'live survey' => [SurveyMother::createLive(), false],
            'closed survey' => [SurveyMother::createClosed(), true],
        ];
    }
}
