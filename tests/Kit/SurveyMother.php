<?php

namespace Tests\Kit;

use App\Entity\Survey;

class SurveyMother
{
    public static function createAny(): Survey
    {
        return new Survey('Some name', 'email@example.com');
    }

    public static function createWithName(string $surveyName): Survey
    {
        return new Survey($surveyName, 'email@example.com');
    }

    public static function createLive(string $surveyName = 'Some name'): Survey
    {
        $survey = self::createWithName($surveyName);
        $survey->goLive();

        return $survey;
    }

    public static function createClosed(string $surveyName = 'Some name'): Survey
    {
        $survey = self::createLive($surveyName);
        $survey->close();

        return $survey;
    }

    public static function createNew(): Survey
    {
        return new Survey('Some name', 'email@example.com');
    }
}
