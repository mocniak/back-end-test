<?php

namespace Tests\Behat;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

final class WebClient
{
    private KernelInterface $kernel;
    private ?Response $response;

    public function __construct(KernelInterface $kernel)
    {
        $this->response = null;
        $this->kernel = $kernel;
    }

    public function fetch(string $path, string $method = 'GET', array $body = null, array $headers = null)
    {
        $request = Request::create($path, $method, [], [], [], [], $body === null ? null : json_encode($body));

        if ($headers !== null){
            foreach ($headers as $header) {
                $request->headers->add($header);
            }
        }
        $this->response = $this->kernel->handle($request);
    }

    public function getLatestResponseContent(): array
    {
        return json_decode($this->response->getContent(), true);
    }

    public function getLatestResponseCode(): int
    {
        return $this->response->getStatusCode();
    }
}
