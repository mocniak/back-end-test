<?php

namespace Tests\Behat;

use Behat\Behat\Context\Context;
use Webmozart\Assert\Assert;

final class ApiContext implements Context
{
    private WebClient $webClient;

    public function __construct(WebClient $webClient)
    {
        $this->webClient = $webClient;
    }

    /**
     * @Then I see :numberOfResults results
     */
    public function iSeeResults(string $numberOfResults)
    {
        Assert::eq(count($this->webClient->getLatestResponseContent()), (int)$numberOfResults);
    }

    /**
     * @Then I get :statusCode response code
     */
    public function iGetResponseCode(string $statusCode)
    {
        Assert::eq($this->webClient->getLatestResponseCode(), (int)$statusCode);
    }

    /**
     * @Then I see on the page that :fieldName is :fieldValue
     */
    public function iSeeOnThePageThatIs(string $fieldName, string $fieldValue)
    {
        Assert::eq($fieldValue, $this->webClient->getLatestResponseContent()[$fieldName]);
    }
}
