<?php

declare(strict_types=1);

namespace Tests\Behat;

use Behat\Behat\Context\Context;
use Symfony\Bundle\FrameworkBundle\Test\MailerAssertionsTrait;
use Symfony\Component\Mailer\Event\MessageEvent;
use Symfony\Component\Mailer\EventListener\MessageLoggerListener;
use Symfony\Component\Mime\Address;
use Webmozart\Assert\Assert;

final class MailContext implements Context
{
    use MailerAssertionsTrait;

    public function __construct(
        private readonly MessageLoggerListener $messageLoggerListener
    )
    {
    }

    /**
     * @Then there is a report notification email sent for :reportEmailAddress
     */
    public function thereIsAnReportNotificationEmailSentFor(string $reportEmailAddress)
    {
        $messagesSentToReportEmailAddress = array_values(array_filter(
            $this->messageLoggerListener->getEvents()->getEvents(),
            function (MessageEvent $event) use ($reportEmailAddress) {
                $isEmailSent = !$event->isQueued();
                $recipients = array_map(
                    fn(Address $recipient) => $recipient->getAddress(),
                    $event->getEnvelope()->getRecipients()
                );
                return $isEmailSent && in_array($reportEmailAddress, $recipients);
            }));

        Assert::count($messagesSentToReportEmailAddress, 1);
    }
}
