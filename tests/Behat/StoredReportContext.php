<?php

declare(strict_types=1);

namespace Tests\Behat;

use App\Repository\ReportRepository;
use App\Repository\SurveyRepository;
use Behat\Behat\Context\Context;
use Webmozart\Assert\Assert;

final class StoredReportContext implements Context
{
    public function __construct(
        private readonly SurveyRepository $surveyRepository,
        private readonly ReportRepository $reportRepository,
    )
    {
    }

    /**
     * @Then report for survey :surveyName is saved
     */
    public function reportForSurveyIsSaved(string $surveyName)
    {
        $survey = $this->surveyRepository->findOneBy(['name' => $surveyName]);
        Assert::notNull($this->reportRepository->findOneBy(['surveyId' => $survey->id]));
    }
}
