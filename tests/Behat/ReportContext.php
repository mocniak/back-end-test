<?php

declare(strict_types=1);

namespace Tests\Behat;

use App\Repository\SurveyRepository;
use App\View\ReportView;
use Behat\Behat\Context\Context;
use Webmozart\Assert\Assert;

final class ReportContext implements Context
{
    public function __construct(
        private readonly SurveyRepository $surveyRepository,
        private readonly ReportView $reportView
    )
    {
    }

    /**
     * @Given survey :surveyName has answer with quality :quality
     */
    public function surveyHasAnswer(string $surveyName, string $quality)
    {
        $survey = $this->surveyRepository->findOneBy(['name' => $surveyName]);
        $survey->addAnswer((int)$quality, null);
        $this->surveyRepository->save($survey, true);
    }

    /**
     * @Then report for survey :surveyName number of answers is :numberOfAnswers
     */
    public function reportForSurveyNumberOfAnswersIs(string $surveyName, string $numberOfAnswers)
    {
        $survey = $this->surveyRepository->findOneBy(['name' => $surveyName]);
        $report = $this->reportView->get($survey->id);
        Assert::eq((int)$numberOfAnswers, $report->numberOfAnswers);
    }

    /**
     * @Then report for survey :surveyName quality is :quality
     */
    public function reportForSurveyQualityIs($surveyName, $quality)
    {
        $survey = $this->surveyRepository->findOneBy(['name' => $surveyName]);
        $report = $this->reportView->get($survey->id);
        Assert::eq((int)$quality, $report->quality);
    }

}
