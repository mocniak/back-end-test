<?php

declare(strict_types=1);

namespace Tests\Behat;

use App\Entity\Survey;
use App\Message\SurveyClosedEvent;
use App\Repository\SurveyRepository;
use Behat\Behat\Context\Context;
use Symfony\Component\Messenger\MessageBusInterface;
use Tests\Kit\SurveyMother;
use Webmozart\Assert\Assert;

final class SurveyContext implements Context
{
    public function __construct(
        private readonly SurveyRepository $repository,
        private readonly WebClient $webClient,
        private readonly MessageBusInterface $messageBus
    )
    {
    }

    /**
     * @When I create new survey
     */
    public function iCreateNewSurvey()
    {
        $this->webClient->fetch('survey', 'POST',
            [
                'name' => 'some name',
                'reportEmail' => 'email@example.com'
            ]
        );
    }

    /**
     * @Then I see in response new survey id
     */
    public function iSeeInResponseNewSurveyId()
    {
        $result = $this->webClient->getLatestResponseContent();
        Assert::string($result['id']);
    }

    /**
     * @Given there is a new survey :surveyName
     */
    public function thereIsANewSurvey(string $surveyName)
    {
        $this->repository->save(SurveyMother::createWithName($surveyName), true);
    }

    /**
     * @When I make :surveyName survey live
     */
    public function iOpen(string $surveyName)
    {
        $survey = $this->repository->findOneBy(['name' => $surveyName]);
        $this->webClient->fetch('survey/' . $survey->id->toString() . '/goLive', 'POST');
    }

    /**
     * @Then I see that survey :surveyName is opened
     */
    public function theSurveyIsOpened(string $surveyName)
    {
        $result = $this->webClient->getLatestResponseContent();
        Assert::eq($result['status'], 'opened');
    }

    /**
     * @Then survey :surveyName is live
     */
    public function surveyIsLive(string $surveyName)
    {
        $survey = $this->repository->findOneBy(['name' => $surveyName]);
        Assert::eq($survey->status(), Survey::STATUS_LIVE);
    }

    /**
     * @Given there is a live survey :surveyName
     */
    public function thereIsALiveSurvey(string $surveyName)
    {
        $this->repository->save(SurveyMother::createLive($surveyName), true);
    }

    /**
     * @Given there is a closed survey :surveyName
     */
    public function thereIsAClosedSurvey(string $surveyName)
    {
        $this->repository->save(SurveyMother::createClosed($surveyName), true);
    }

    /**
     * @When I close :surveyName survey
     */
    public function iCloseSurvey(string $surveyName)
    {
        $survey = $this->repository->findOneBy(['name' => $surveyName]);
        $this->webClient->fetch('survey/' . $survey->id->toString() . '/close', 'POST');
    }

    /**
     * @Then survey :surveyName is closed
     */
    public function surveyIsClosed(string $surveyName)
    {
        $survey = $this->repository->findOneBy(['name' => $surveyName]);
        Assert::eq($survey->status(), Survey::STATUS_CLOSED);
    }

    /**
     * @When the survey :surveyName was closed
     */
    public function theSurveyIsClosed(string $surveyName)
    {
        $survey = $this->repository->findOneBy(['name' => $surveyName]);
        $this->messageBus->dispatch(new SurveyClosedEvent($survey->id));
    }

    /**
     * @Given report recipient for :surveyName is :emailAddress
     */
    public function reportRecipientForIs($surveyName, $emailAddress)
    {
        $survey = $this->repository->findOneBy(['name' => $surveyName]);
        $survey->changeReportEmail($emailAddress);
        $this->repository->save($survey, true);
    }

    /**
     * @When I answer :surveyName survey
     */
    public function iAnswerSurvey(string $surveyName)
    {
        $survey = $this->repository->findOneBy(['name' => $surveyName]);
        $this->webClient->fetch('survey/' . $survey->id . '/answer', 'POST',
            [
                'quality' => 0,
                'comment' => 'Derp'
            ]
        );
    }

    /**
     * @Then survey :surveyName has :numberOfAnswers answer/answers saved
     */
    public function surveyHasOneAnswerSaved(string $surveyName, string $numberOfAnswers)
    {
        $survey = $this->repository->findOneBy(['name' => $surveyName]);
        Assert::count($survey->getAnswers(), (int)$numberOfAnswers);
    }

}
