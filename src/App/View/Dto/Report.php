<?php

namespace App\View\Dto;

class Report
{
    public function __construct(
        public readonly int $numberOfAnswers,
        public readonly int $quality,
    )
    {
    }
}
