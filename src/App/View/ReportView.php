<?php

namespace App\View;

use App\View\Dto\Report;
use Doctrine\DBAL\Connection;
use Ramsey\Uuid\UuidInterface;

class ReportView
{
    public function __construct(private readonly Connection $connection)
    {
    }

    public function get(UuidInterface $id): Report
    {
        $queryBuilder = $this->connection->createQueryBuilder();

        $queryBuilder
            ->select('count(1)')
            ->from('answer', 'a')
            ->where('survey_id = :value')
            ->setParameter('value', $id->toString());

        $numberOfAnswers = $queryBuilder->fetchAllAssociative()[0]['count'];

        $queryBuilder
            ->select('AVG(quality) as quality')
            ->from('answer', 'a')
            ->where('survey_id = :value')
            ->setParameter('value', $id->toString());

        $averageQuality = $queryBuilder->fetchAllAssociative()[0]['quality'];

        return new Report($numberOfAnswers, (int) $averageQuality);
    }
}
