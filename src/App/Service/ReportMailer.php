<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\ReportRepository;
use App\Repository\SurveyRepository;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

final class ReportMailer
{
    public function __construct(
        private readonly SurveyRepository $surveyRepository,
        private readonly ReportRepository $reportRepository,
        private readonly MailerInterface $mailer,
    )
    {
    }

    public function send(UuidInterface $surveyId): void
    {
        $survey = $this->surveyRepository->find($surveyId);
        $report = $this->reportRepository->findOneBy(['surveyId' => $surveyId]);
        $email = (new Email())
            ->from('hello@example.com')
            ->to($survey->reportEmail())
            ->subject(sprintf('Report for survey "%s" is here!', $survey->name()))
            ->text(
                sprintf(
                    'There should be a link to generated report but just id is also fine ;) - "%s"',
                    $report->id,
                ),
            );

        $this->mailer->send($email);
    }
}
