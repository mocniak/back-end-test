<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Survey;
use App\Repository\SurveyRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class AnswerController extends AbstractController
{
    public function __construct(private readonly SurveyRepository $surveyRepository)
    {
    }

    #[Route('/survey/{id}/answer', methods: 'POST')]
    #[ParamConverter('survey', Survey::class)]
    public function create(Survey $survey, Request $request): JsonResponse
    {
        $payload = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        try {
            $survey->addAnswer($payload['quality'], $payload['comment']);
        } catch (\DomainException $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_CONFLICT);
        }
        $this->surveyRepository->save($survey, true);

        return new JsonResponse([], Response::HTTP_CREATED);
    }
}
