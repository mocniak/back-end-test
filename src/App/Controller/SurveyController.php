<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Survey;
use App\Message\SurveyClosedEvent;
use App\Repository\SurveyRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/survey')]
class SurveyController extends AbstractController
{
    public function __construct(
        private readonly MessageBusInterface $messageBus,
        private readonly SurveyRepository $surveyRepository,
    )
    {
    }

    #[Route(methods: 'GET')]
    public function index(): JsonResponse
    {
        $surveys = $this->getDoctrine()->getRepository(Survey::class)->findAll();

        return $this->json(array_map(function (Survey $survey) {
            return [
                'id' => $survey->id,
                'name' => $survey->name(),
                'reportEmail' => $survey->reportEmail(),
                'status' => $survey->status(),
                'answers' => $survey->getAnswers(),
            ];
        }, $surveys));
    }

    #[Route(methods: 'POST')]
    public function create(Request $request): JsonResponse
    {
        $payload = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $newSurvey = new Survey($payload['name'], $payload['reportEmail']);
        $this->surveyRepository->save($newSurvey, true);

        return new JsonResponse(['id' => $newSurvey->id->toString()], Response::HTTP_CREATED);
    }

    #[Route('/{id}/goLive', methods: 'POST')]
    #[ParamConverter('survey', Survey::class)]
    public function goLive(Survey $survey): JsonResponse
    {
        try {
            $survey->goLive();
        } catch (\DomainException $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_CONFLICT);
        }
        $this->surveyRepository->save($survey, true);

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}/close', methods: 'POST')]
    #[ParamConverter('survey', Survey::class)]
    public function close(Survey $survey): JsonResponse
    {
        try {
            $survey->close();
        } catch (\DomainException $exception) {
            return new JsonResponse(['error' => $exception->getMessage()], Response::HTTP_CONFLICT);
        }
        $this->surveyRepository->save($survey, true);
        $this->messageBus->dispatch(new SurveyClosedEvent($survey->id));

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }
}
