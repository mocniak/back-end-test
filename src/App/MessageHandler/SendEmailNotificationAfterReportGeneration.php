<?php

namespace App\MessageHandler;

use App\Message\ReportGeneratedEvent;
use App\Service\ReportMailer;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class SendEmailNotificationAfterReportGeneration
{
    public function __construct(
        private readonly ReportMailer $reportMailer,
        private readonly LoggerInterface $logger,
    )
    {
    }

    public function __invoke(ReportGeneratedEvent $reportGeneratedEvent): void
    {
        try {
            $this->reportMailer->send($reportGeneratedEvent->surveyId);
        } catch (\Throwable $exception) {
            $this->logger->error(
                'Mailer failed to send an email', [
                    'error' => $exception,
                    'surveyId' => $reportGeneratedEvent->surveyId
                ]
            );
        }
    }
}
