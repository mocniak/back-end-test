<?php

namespace App\MessageHandler;

use App\Entity\Report;
use App\Message\ReportGeneratedEvent;
use App\Message\SurveyClosedEvent;
use App\Repository\ReportRepository;
use App\View\ReportView;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
class GenerateReportAfterSurveyClosure
{
    public function __construct(
        private readonly MessageBusInterface $messageBus,
        private readonly ReportView $reportView,
        private readonly ReportRepository $reportRepository,
    )
    {
    }

    public function __invoke(SurveyClosedEvent $surveyClosedEvent): void
    {
        $report = $this->reportView->get($surveyClosedEvent->surveyId);
        $this->reportRepository->save(new Report(
            $surveyClosedEvent->surveyId,
            $report->numberOfAnswers,
            $report->quality, []), true);
        $this->messageBus->dispatch(new ReportGeneratedEvent($surveyClosedEvent->surveyId));
    }
}
