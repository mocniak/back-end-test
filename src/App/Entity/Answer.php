<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

#[ORM\Entity]
class Answer
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    public readonly UuidInterface $id;

    #[ORM\Column]
    #[Assert\Range(min: -2, max: 2)]
    public readonly  int $quality;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    public readonly  ?string $comment;

    #[ORM\ManyToOne(inversedBy: 'answer')]
    #[ORM\JoinColumn(nullable: false)]
    #[Ignore]
    private Survey $survey;

    public function __construct(Survey $survey, int $quality, ?string $comment)
    {
        $this->id = Uuid::uuid4();
        $this->quality = $quality;
        $this->survey = $survey;
        $this->comment = $comment;
    }

    #[Assert\Callback]
    public function validateComment(ExecutionContextInterface $context, $payload): void
    {
        // require comment when negative recommendation
        if ($this->getComment() === null && in_array($this->getQuality(), [-2, -1], true)) {
            $context
                ->buildViolation('Comment is required for poor quality')
                ->atPath('comment')
                ->addViolation();
        } else if ($this->getComment() !== null && in_array($this->getQuality(), [0, 1, 2], true)) {
            $context
                ->buildViolation('There should be no comment when good quality')
                ->atPath('comment')
                ->addViolation();
        }
    }
}
