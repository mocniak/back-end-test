<?php

namespace App\Entity;

use App\Entity\Exception\SurveyNotAllowingAnswersException;
use App\Entity\Exception\SurveyLifecycleException;
use App\Repository\SurveyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SurveyRepository::class)]
#[UniqueEntity(fields: 'name', errorPath: 'name')]
class Survey
{
    public const STATUS_NEW = 'new';
    public const STATUS_LIVE = 'live';
    public const STATUS_CLOSED = 'closed';

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    public readonly UuidInterface $id;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    private string $name;

    #[ORM\Column(length: 32)]
    private string $status;

    #[ORM\OneToMany(mappedBy: 'survey', targetEntity: Answer::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $answers;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank]
    #[Assert\Email]
    private string $reportEmail;

    public function __construct(string $name, string $reportEmail)
    {
        $this->id = Uuid::uuid4();
        $this->status = self::STATUS_NEW;
        $this->name = $name;
        $this->answers = new ArrayCollection();
        $this->reportEmail = $reportEmail;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function status(): string
    {
        return $this->status;
    }

    /**
     * @return Collection<int, Answer>
     */
    public function getAnswers(): Collection
    {
        return clone $this->answers;
    }

    public function addAnswer(int $quality, ?string $comment): void
    {
        if ($this->status !== self::STATUS_LIVE) {
            throw new SurveyNotAllowingAnswersException('Only live surveys can be answered');
        }
        $this->answers->add(new Answer($this, $quality, $comment));
    }

    public function changeReportEmail(string $emailAddress): void
    {
        $this->reportEmail = $emailAddress;
    }

    public function reportEmail(): string
    {
        return $this->reportEmail;
    }

    public function goLive(): void
    {
        if ($this->status !== self::STATUS_NEW) {
            throw new SurveyLifecycleException('Only new surveys can be set live');
        }
        $this->status = self::STATUS_LIVE;
    }

    public function close(): void
    {
        if ($this->status === self::STATUS_CLOSED) {
            throw new SurveyLifecycleException('Survey is already closed');
        }
        $this->status = self::STATUS_CLOSED;
    }
}
