<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

#[ORM\Entity(repositoryClass: ReportRepository::class)]
class Report
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    public readonly UuidInterface $id;

    #[ORM\Column(type: 'uuid')]
    public readonly UuidInterface $surveyId;

    #[ORM\Column]
    public readonly int $numberOfAnswers;

    #[ORM\Column]
    public readonly int $quality;

    #[ORM\Column(type: Types::JSON)]
    public readonly array $comments;

    #[ORM\Column]
    public readonly \DateTimeImmutable $generatedAt;

    public function __construct(UuidInterface $surveyId, int $numberOfAnswers, int $quality, array $comments)
    {
        $this->id = Uuid::uuid4();
        $this->surveyId = $surveyId;
        $this->numberOfAnswers = $numberOfAnswers;
        $this->quality = $quality;
        $this->comments = $comments;
        $this->generatedAt = new \DateTimeImmutable('now');
    }
}
