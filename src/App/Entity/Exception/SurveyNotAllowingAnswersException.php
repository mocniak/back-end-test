<?php

namespace App\Entity\Exception;

class SurveyNotAllowingAnswersException extends \DomainException
{
}
