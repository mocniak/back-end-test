<?php

namespace App\Message;

use Ramsey\Uuid\UuidInterface;

class ReportGeneratedEvent
{
    public function __construct(public readonly UuidInterface $surveyId)
    {
    }
}
