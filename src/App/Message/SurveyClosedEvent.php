<?php

namespace App\Message;

use Ramsey\Uuid\UuidInterface;

class SurveyClosedEvent
{
    public function __construct(public readonly UuidInterface $surveyId)
    {
    }
}
