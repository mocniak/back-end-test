Feature:
    In order to limit a time when survey can be answered
    As a user
    I want to have ability to open and close my surveys

    Scenario: Survey can be created
        When I create new survey
        Then I get 201 response code
        And I see in response new survey id

    Scenario: New surveys can be sent live
        Given there is a new survey "Which Pokemon is your favourite?"
        When I make "Which Pokemon is your favourite?" survey live
        Then I get 204 response code
        And survey "Which Pokemon is your favourite?" is live

    Scenario: Live surveys can be closed
        Given there is a live survey "Which Pokemon is your favourite?"
        When I close "Which Pokemon is your favourite?" survey
        Then I get 204 response code
        And survey "Which Pokemon is your favourite?" is closed

    Scenario: Live surveys cannot be set live again
        Given there is a live survey "Which Pokemon is your favourite?"
        When I make "Which Pokemon is your favourite?" survey live
        Then I get 409 response code

    Scenario: Closed surveys cannot be set live
        Given there is a closed survey "Which Pokemon is your favourite?"
        When I make "Which Pokemon is your favourite?" survey live
        Then I get 409 response code

    Scenario: Closed surveys cannot be closed again
        Given there is a closed survey "Which Pokemon is your favourite?"
        When I close "Which Pokemon is your favourite?" survey
        Then I get 409 response code
