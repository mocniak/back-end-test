Feature:
    In order to save survey results and send them to the user
    As an app
    I want to create reports and send notification email after a survey is closed

    Scenario: Closing the survey
        Given there is a live survey "Some survey"
        And report recipient for "Some survey" is "user@example.com"
        When the survey "Some survey" was closed
        Then report for survey "Some survey" is saved
        And there is a report notification email sent for "user@example.com"
