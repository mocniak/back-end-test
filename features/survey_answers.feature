Feature:
    In order to have my surveys relevant
    As a user
    I want to make my surveys answerable

    Scenario: Users can answer to live surveys
        Given there is a live survey "How was your day?"
        When I answer "How was your day?" survey
        Then I get 201 response code
        And survey "How was your day?" has 1 answer saved

    Scenario: Users cannot answer to closed surveys
        Given there is a closed survey "How was your day?"
        When I answer "How was your day?" survey
        Then I get 409 response code
        And survey "How was your day?" has 0 answers saved

    Scenario: Users cannot answer to new surveys
        Given there is a new survey "How was your day?"
        When I answer "How was your day?" survey
        Then I get 409 response code
        And survey "How was your day?" has 0 answers saved
