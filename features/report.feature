Feature:
    In order to get a results of a survey
    As a user
    I want to have survey reports available

    Scenario: Report for survey with one answer
        Given there is a live survey "Some survey"
        And survey "Some survey" has answer with quality 0
        Then report for survey "Some survey" number of answers is 1
        And report for survey "Some survey" quality is 0

    Scenario: Report for survey with many answers
        Given there is a live survey "Some survey"
        And survey "Some survey" has answer with quality 0
        And survey "Some survey" has answer with quality 1
        And survey "Some survey" has answer with quality 2
        Then report for survey "Some survey" number of answers is 3
        And report for survey "Some survey" quality is 1

    Scenario: Quality in report is an average of all quality values in answers rounded down
        Given there is a live survey "Some survey"
        And survey "Some survey" has answer with quality 0
        And survey "Some survey" has answer with quality 1
        Then report for survey "Some survey" quality is 0

    Scenario: Answers from other surveys are not included in report for a given one
        Given there is a live survey "Some survey"
        And there is a live survey "Another survey"
        And survey "Some survey" has answer with quality 0
        And survey "Some survey" has answer with quality 1
        And survey "Another survey" has answer with quality "-1"
        Then report for survey "Some survey" quality is 0
        And report for survey "Some survey" number of answers is 2
        And report for survey "Another survey" number of answers is 1
